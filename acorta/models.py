from django.db import models

class URL(models.Model):
    short_code = models.CharField(max_length=50, unique=True)
    long_url = models.URLField()

    def __str__(self):
        return self.short_code
