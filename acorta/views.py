from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import URL
import uuid

def index(request):
    urls = URL.objects.all()
    return render(request, 'index.html', {'urls': urls})

def shorten_url(request):
    if request.method == 'POST':
        long_url = request.POST.get('url')
        short_code = request.POST.get('short')
        if not long_url:
            return HttpResponse("Invalid URL", status=400)
        if not short_code:
            # Generate a sequential short code
            short_code = str(URL.objects.count() + 1)
        elif URL.objects.filter(short_code=short_code).exists():
            return HttpResponse("Short code already exists", status=400)
        url = URL(short_code=short_code, long_url=long_url)
        url.save()
        return redirect('index')
    return HttpResponse("Method not allowed", status=405)

def redirect_to_url(request, short_code):
    try:
        url = URL.objects.get(short_code=short_code)
        return redirect(url.long_url)
    except URL.DoesNotExist:
        return HttpResponse("URL not found", status=404)
